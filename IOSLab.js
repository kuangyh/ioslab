var React = require('react-native');
var {
  View,
  Text,
  AppRegistry,
  StyleSheet,
  requireNativeComponent
} = React;

var MyMap = requireNativeComponent('MyMap', null);

var App = React.createClass({
  render: function() {
    return (
      <View style={styles.container}>
        <Text>Hello World</Text>
        <MyMap style={styles.maps}></MyMap>
      </View>);
  },
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  maps: {
    width: 100,
    height: 100,
  }
});

AppRegistry.registerComponent('IOSLab', () => App);
