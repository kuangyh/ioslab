//: Playground - noun: a place where people can play

import UIKit

var style = NSMutableParagraphStyle()
style.alignment = .Left
style.lineBreakMode = .ByWordWrapping

let text = "Hello World Hello World Hello World Hello World Hello World Hello World"
let s = NSAttributedString(string: text, attributes: [NSParagraphStyleAttributeName: style])
let rect = s.boundingRectWithSize(CGSize(width: 320, height: 240), options: [.UsesLineFragmentOrigin], context: nil)

