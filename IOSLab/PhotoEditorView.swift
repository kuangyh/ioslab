//
//  PhotoEditorView.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 10/28/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit

class PhotoEditorView: UIView {
    private let _label = UILabel(frame: CGRectZero)

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.backgroundColor = UIColor.whiteColor()
        
        _label.text = "Hello World"
        _label.textColor = UIColor.blackColor()
        self.addSubview(_label)
    }
    
    override func layoutSubviews() {
        _label.sizeToFit()
        _label.center = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
