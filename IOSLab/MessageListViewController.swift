//
//  MessageListViewController.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 11/8/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit

class MessageViewDataSync {
    let view: MessageListView
    var reading = false
    
    init(view: MessageListView) {
        self.view = view
        self.reading = true
        MessageService.shared.getMessages(MessageListCursor(threadId: 1000, messageId: 10000, direction: -1),limit: 50).then { messages -> Void in
            self.reading = false
            self.view.data = messages
        }
    }
    
    func readMore() {
        if self.reading {
            return
        }
        var cursor = MessageListCursor(threadId: 1000, messageId: 10000, direction: -1)
        if self.view.data.count > 0 {
            let first = self.view.data[0]
            cursor.threadId = first.threadId
            cursor.messageId = first.messageId - 1
        }
        self.reading = true
        MessageService.shared.getMessages(cursor, limit: 50).then { messages -> Void in
            self.reading = false
            self.view.data = messages + self.view.data
        }
    }
}

class MessageListViewController: UIViewController, MessageListDelegate {
    var _messageListView: MessageListView?
    var _sync: MessageViewDataSync?
    
    override func loadView() {
        self.view = UIView(frame: UIScreen.mainScreen().bounds)
        self.view.backgroundColor = UIColor.whiteColor()
        
        let messageListView = MessageListView(frame: self.view.frame)
        messageListView.messageListDelegate = self
        self.view.addSubview(messageListView)
        _messageListView = messageListView
        _sync = MessageViewDataSync(view: messageListView)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func wantsMore() {
        _sync?.readMore()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
