//
//  ReactWebView.swift
//  WKWebKit enhancement that enables communication between web content and view.
//  It helps developing React UIComponent using Web technologies.
//
//  Created by Yuheng Kuang on 9/24/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit
import PromiseKit
import WebKit

// Keep weak reference to target to avoid cycle references.
class ScriptMessageProxy: NSObject, WKScriptMessageHandler {
    weak var target: AnyObject?
    let selector: Selector

    init(target: AnyObject, selector: Selector) {
        self.target = target
        self.selector = selector
    }
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        self.target?.performSelector(selector, withObject: message.body)
    }
}

class WebUIComponent: WKWebView {
    let _entryPoint: String
    let _contentController: WKUserContentController

    init(frame: CGRect, entryPoint: String) {
        _entryPoint = entryPoint
        _contentController = WKUserContentController()
        let conf = WKWebViewConfiguration()
        conf.userContentController = _contentController
        conf.allowsInlineMediaPlayback = true
        conf.requiresUserActionForMediaPlayback = false
        super.init(frame: frame, configuration: conf)
        if let url = NSBundle.mainBundle().URLForResource(entryPoint, withExtension: "html", subdirectory: "Data/www") {
            self.loadRequest(NSURLRequest(URL: url))
        } else {
            NSLog("Invalid WebUIComponent entry point %@", entryPoint)
        }
    }

    func listen(target: AnyObject, selector: Selector, channel: String) {
        _contentController.addScriptMessageHandler(ScriptMessageProxy(target: target, selector: selector), name: channel)
    }

    func _sendSerializedJSON(data: NSData) -> Bool {
        let json = String(data: data, encoding: NSUTF8StringEncoding) ?? "null";
        let script = (
            "(function(d){if(window.__receive){__receive(d);}else{" +
            "(window.__pending=window.__pending||[]).push(d);};return null;})(\(json))")
        self.evaluateJavaScript(script) { (result, error) -> Void in
            if error != nil {
                NSLog("WebUIComponent send Error %@", error.debugDescription)
            }
        }
        return true
    }

    func send(channel: String, data: AnyObject) -> Bool {
        let msg = ["channel": channel, "payload": data]
        if let serizlied = try? NSJSONSerialization.dataWithJSONObject(msg, options: NSJSONWritingOptions()) {
            return _sendSerializedJSON(serizlied)
        }
        NSLog("Failed to serialize send payload.")
        return false
    }
}
