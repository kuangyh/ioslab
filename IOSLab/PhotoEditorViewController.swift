//
//  PhotoEditorViewController.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 10/28/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit
import PromiseKit
import AVFoundation
import MobileCoreServices

class PhotoEditorViewController: UIViewController {
    var _captureSesion: AVCaptureSession?
    var _captureInited = false
    let _captureOutput = AVCaptureStillImageOutput()
    let _captureQueue: dispatch_queue_t = dispatch_queue_create("PhotoEditorViewController", nil)
    var _takePhotoButton: UIButton?
    let _picker = UIImagePickerController()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        initCaptureSession()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = UIView(frame: UIScreen.mainScreen().bounds)
        self.view.backgroundColor = UIColor.redColor()
        
        _takePhotoButton = UIButton(frame: CGRectZero)
        if let button = _takePhotoButton {
            button.setTitle("Take Photo", forState: .Normal)
            button.titleLabel?.font = UIFont.systemFontOfSize(24)
            button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            self.view.addSubview(button)
            button.sizeToFit()
            button.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height - 30)
            button.addTarget(self, action: "takePhoto:", forControlEvents: UIControlEvents.TouchUpInside)
            button.layer.zPosition = 1000
        }
        let previewLayer = AVCaptureVideoPreviewLayer(session: self._captureSesion)
        previewLayer.frame = self.view.bounds
        self.view.layer.addSublayer(previewLayer)
    }
    
    override func viewWillAppear(animated: Bool) {
        dispatch_async(self._captureQueue) {
            self._captureSesion?.startRunning()
        }
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        dispatch_async(self._captureQueue) {
            self._captureSesion?.stopRunning()
        }
        super.viewDidDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func takePhoto(sender: AnyObject?) {
        _picker.sourceType = .PhotoLibrary
        _picker.mediaTypes = [kUTTypeImage as String]
        self.presentViewController(_picker, animated: true, completion: nil)
        
        dispatch_async(self._captureQueue) {
            let conn = self._captureOutput.connectionWithMediaType(AVMediaTypeVideo)
            conn.videoOrientation = AVCaptureVideoOrientation.Portrait
            self._captureOutput.captureStillImageAsynchronouslyFromConnection(conn) {
                buffer, error in
                if error != nil {
                    NSLog("Capture error %@", error)
                    return
                }
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
                
                if let _ = UIImage(data: imageData) {
                    NSLog("Wooho!")
                }
            }
        }
    }
    
    func initCaptureSession() {
        _captureInited = true
        let authorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        if authorizationStatus == .Denied || authorizationStatus == .Restricted {
            NSLog("Camera not allowed :-(")
            return
        }
        
        let session = AVCaptureSession()
        var hasInput: Bool = false
        if let availableCameraDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo) as?[AVCaptureDevice] {
            for device in availableCameraDevices {
                if device.position != .Back {
                    continue
                }
                guard let deviceInput = try? AVCaptureDeviceInput(device: device) else {
                    continue
                }
                if session.canAddInput(deviceInput) {
                    session.addInput(deviceInput)
                    hasInput = true
                    break
                }
            }
        }
        if !hasInput {
            NSLog("No camera :-(")
            return
        }
        if session.canAddOutput(_captureOutput) {
            session.addOutput(_captureOutput)
        } else {
            NSLog("Don't allow to add output")
            return
        }
        session.sessionPreset = AVCaptureSessionPresetPhoto
        self._captureSesion = session
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
