//
//  ZenCameraView.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 10/29/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit
import AVFoundation
import SnapKit

@objc protocol ZenCameraViewDelegate {
    optional func zenCameraView(takesPhoto view: ZenCameraView)
}

class ZenCameraView: UIView {
    weak var delegate: ZenCameraViewDelegate?
    var previewLayer: AVCaptureVideoPreviewLayer? = nil {
        didSet {
            if let oldLayer = oldValue {
                oldLayer.removeFromSuperlayer()
            }
            if let newLayer = previewLayer {
                newLayer.frame = self.frame
                self.layer.addSublayer(newLayer)
            }
        }
    }
    
    private let _takePhotoButton = UIButton(frame: CGRectZero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(_takePhotoButton)
        _takePhotoButton.addTarget(self, action: "takePhotoButtonTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        _setLayoutAndStyles()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func takePhotoButtonTapped(button: UIButton) {
        self.delegate?.zenCameraView?(takesPhoto: self)
    }
    
    func linkCaptureSession(session: AVCaptureSession) {
        let previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer.frame = self.frame
        self.layer.addSublayer(previewLayer)
    }
    
    private func _setLayoutAndStyles() {
        _takePhotoButton.layer.zPosition = 1000
        if let path = NSBundle.mainBundle().pathForResource("take_photo_button", ofType: "png", inDirectory: "Data/Images") {
            _takePhotoButton.setImage(UIImage(contentsOfFile: path), forState: .Normal)
        }
        _takePhotoButton.snp_makeConstraints { make in
            make.bottom.equalTo(self).offset(-30)
            make.centerX.equalTo(self.snp_centerX)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
