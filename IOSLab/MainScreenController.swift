//
//  MainScreenViewController.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 9/22/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit

class Lab {
    let name: String
    let cls: UIViewController.Type
    var _controller: UIViewController?
    
    init(name: String, cls: UIViewController.Type) {
        self.name = name
        self.cls = cls
    }
    
    func getController() -> UIViewController {
        if let ctrl = self._controller {
            return ctrl
        }
        self._controller = self.cls.init()
        return self._controller!
    }
}

class MainScreenController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var _tableView: UITableView?
    var _labs = [
        Lab(name: "ZenImagePikcer", cls: ZenImagePickerController.self),
        Lab(name: "Photo Editor", cls: PhotoEditorViewController.self),
        Lab(name: "Photo Converter", cls: PhotoConverterPageController.self),
        Lab(name: "Web View", cls: WebViewPageController.self),
        Lab(name: "Debug Console", cls: DebugConsoleViewController.self),
        Lab(name: "Message List", cls: MessageListViewController.self)
    ]
    
    override func loadView() {
        let navBarHeight = self.navigationController?.navigationBar.frame.maxY ?? 0
        let screenBounds = UIScreen.mainScreen().bounds
        _tableView = UITableView(frame: CGRect(x: 0, y: navBarHeight, width: screenBounds.width, height: screenBounds.height - navBarHeight))
        _tableView?.delegate = self
        _tableView?.dataSource = self
        self.view = _tableView
    }
    
    override func viewDidAppear(animated: Bool) {

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self._labs.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = _tableView?.dequeueReusableCellWithIdentifier("cell") ?? (
            UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell"))
        cell.textLabel?.text = self._labs[indexPath.row].name
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let controller = _labs[indexPath.row].getController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
