//
//  DebugConsoleViewController.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 10/29/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit
import Spring

class MockPage: UIViewController {
    let _pageColor: UIColor
    let _title: String
    
    init(title: String, color: UIColor) {
        _title = title
        _pageColor = color
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = UIView(frame: UIScreen.mainScreen().bounds)
        self.view.backgroundColor = _pageColor
        
        let label = UILabel(frame: CGRectZero)
        label.text = _title
        label.textColor = UIColor.blackColor()
        label.font = UIFont.systemFontOfSize(24)
        label.sizeToFit()
        label.center = self.view.center
        
        self.view.addSubview(label)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        NSLog("\(_title) didAppear")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSLog("\(_title) willAppear")
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidAppear(animated)
        NSLog("\(_title) didDisappear")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSLog("\(_title) willDisappear")
    }
}

class ZenPager: UIScrollView, UIScrollViewDelegate {
    var _pages: [UIView] = []
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.delegate = self
        self.scrollEnabled = true
        self.pagingEnabled = true
        self.bounces = true
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        self.panGestureRecognizer.addTarget(self, action: "onPanGesture:")
    }
    
    func setPages(pages: [UIView], scrollToPage: Int = 0, animated: Bool = false) {
        for page in _pages {
            page.removeFromSuperview()
        }
        _pages = pages
        let frameWidth = self.frame.width
        var currentOffset: CGFloat = 0
        for page in _pages {
            page.frame.size = self.frame.size
            page.frame.origin = CGPoint(x: currentOffset, y: 0)
            currentOffset += frameWidth
        }
        self.contentSize = CGSize(width: currentOffset, height: self.frame.height)
        self.scrollToPage(scrollToPage, animated: animated)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        for var i = 0; i < _pages.count; i++ {
            let page = _pages[i]
            if abs(page.frame.origin.x - self.contentOffset.x) < self.frame.width {
                if page.superview != self {
                    page.removeFromSuperview()
                }
                self.addSubview(page)
            } else {
                page.removeFromSuperview()
            }
        }
    }
    
    func onPanGesture(gesture: UIPanGestureRecognizer) {

    }
    
    func scrollToPage(index: Int, animated: Bool) {
        let frameWidth = self.frame.width
        self.setContentOffset(CGPoint(x: CGFloat(index) * frameWidth, y: 0), animated: animated)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class DebugConsoleViewController: UIViewController {
    
    override func loadView() {
        self.view = UIView(frame: UIScreen.mainScreen().bounds)
        
        let cursor = MessageListCursor(threadId: 0, messageId: 0, direction: 1)
        MessageService.shared.getMessages(cursor, limit: 20).then { messages in
            NSLog("Receive messages: \(messages)")
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
