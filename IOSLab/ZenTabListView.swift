//
//  ZenTabListView.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 10/29/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit

@objc protocol ZenTabListViewDelegate {
    optional func zenTabListView(tabListView: ZenTabListView, tabClicked index: Int)
}

class ZenTabListView: UIView {
    let TAB_WIDTH: CGFloat = 75
    let TAB_HEIGHT: CGFloat = 20
    let TAB_MARGIN: CGFloat = 10
    let TAB_FONT_SIZE: CGFloat = 16
    
    private var _tabButtons: [UIButton] = []
    
    weak var delegate: ZenTabListViewDelegate?

    var activeTabIndex: Int = -1 {
        willSet(index) {
            for var i = 0; i < _tabButtons.count; i++ {
                _tabButtons[i].titleLabel?.font = UIFont.systemFontOfSize(TAB_FONT_SIZE, weight: i == index ? UIFontWeightBold : UIFontWeightLight)
            }
        }
    }
    
    var tabs: [String] = [] {
        willSet (newTabs)  {
            for tab in _tabButtons {
                tab.removeTarget(self, action: "tabClicked:", forControlEvents: .TouchUpInside)
                tab.removeFromSuperview()
            }
            _tabButtons = []
            self.activeTabIndex = -1
            for tabText in newTabs {
                let button = UIButton(frame: CGRect(x: 0, y: 0, width: TAB_WIDTH, height: TAB_HEIGHT))
                button.setTitle(tabText, forState: .Normal)
                button.backgroundColor = UIColor.clearColor()
                button.titleLabel?.font = UIFont.systemFontOfSize(TAB_FONT_SIZE, weight: UIFontWeightLight)
                button.titleLabel?.textAlignment = .Center
                button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                button.addTarget(self, action: "tabClicked:", forControlEvents: .TouchUpInside)
                _tabButtons.append(button)
                self.addSubview(button)
            }
        }
    }
    
    func tabClicked(sender: UIView) {
        for var i = 0; i < _tabButtons.count; i++ {
            if _tabButtons[i] == sender {
                self.delegate?.zenTabListView?(self, tabClicked: i)
                break
            }
        }
    }
    
    override func layoutSubviews() {
        let contentWidth = CGFloat(_tabButtons.count) * (TAB_WIDTH + TAB_MARGIN) - TAB_MARGIN
        var offset = (self.bounds.width - contentWidth) / 2
        for tab in self._tabButtons {
            tab.frame = CGRect(x: offset, y: 0, width: TAB_WIDTH, height: TAB_HEIGHT)
            offset += (TAB_WIDTH + TAB_MARGIN)
        }
    }
    
    override func sizeThatFits(size: CGSize) -> CGSize {
        return CGSize(
            width: max(size.width, CGFloat(_tabButtons.count) * (TAB_WIDTH + TAB_MARGIN) - TAB_MARGIN),
            height: max(size.height, TAB_HEIGHT))
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
