//
//  ZenImagePickerController.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 10/29/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit
import SnapKit

@objc protocol ZenImagePickerDelegate {
    func imageDidTaken(image: UIImage, fromPhotoLibrary: Bool)
    func imagePickerCanceled()
}

class ZenImagePickerController: UIPageViewController, UIPageViewControllerDataSource,  UIPageViewControllerDelegate, ZenTabListViewDelegate {

    weak var imagePickerDelegate: ZenImagePickerDelegate? {
        didSet {
            // Pipe up events
            if let ctrl = _pages[0] as? ZenPhotoLibraryController {
                ctrl.delegate = self.imagePickerDelegate
            }
            if let ctrl = _pages[1] as? ZenCameraController {
                ctrl.delegate = self.imagePickerDelegate
            }
            if let ctrl = _pages[2] as? ZenCameraController {
                ctrl.delegate = self.imagePickerDelegate
            }

        }
    }
    
    private var _tabList: ZenTabListView?
    private let _pages = [
        ZenPhotoLibraryController(),
        ZenCameraController(cameraPosition: .Back),
        ZenCameraController(cameraPosition: .Front)
    ]

    init() {
        super.init(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
        
        self.setViewControllers([_pages[1]], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        self.dataSource = self
        self.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func loadView() {
        super.loadView()
        self.view.frame = UIScreen.mainScreen().bounds
        _addTabList()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Page transition
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        for var i = 0; i < self._pages.count; i++ {
            if self._pages[i] == viewController {
                return i + 1 < self._pages.count ? self._pages[i + 1] : nil
            }
        }
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        for var i = 0; i < self._pages.count; i++ {
            if self._pages[i] == viewController {
                return i > 0 ? self._pages[i - 1] : nil
            }
        }
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if self.viewControllers?.count ?? 0 > 0 {
            let curr = self.viewControllers![0]
            for var i = 0; i < self._pages.count; i++ {
                if self._pages[i] == curr {
                    self._tabList?.activeTabIndex = i
                    return
                }
            }
        }
    }
    
    // MAKE: TabList
    private func _addTabList() {
        let tabList = ZenTabListView(frame: CGRectZero)
        tabList.tabs = ["GALLERY", "CAMERA", "SELFIE"]
        tabList.delegate = self
        tabList.activeTabIndex = 1
        self.view.addSubview(tabList)
        tabList.snp_makeConstraints { make in
            make.width.equalTo(self.view.snp_width)
            make.height.equalTo(tabList.TAB_HEIGHT)
            make.bottom.equalTo(self.view.snp_bottom)
        }
        _tabList = tabList
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
