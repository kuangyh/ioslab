//
//  MessageListView.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 11/8/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit

class MessageListCell: UICollectionViewCell {
    let _label: UILabel = UILabel(frame: CGRectZero)
    var messageId: Int = 0
    
    var text: String = "" {
        didSet {
            _label.text = text
            let fitSize = _label.sizeThatFits(CGSize(width: UIScreen.mainScreen().bounds.width, height: 1))
            _label.frame.origin = CGPointZero
            _label.frame.size = fitSize
            self.frame.size = fitSize
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        _label.numberOfLines = 0
        _label.textColor = UIColor.blackColor()
        _label.font = UIFont.systemFontOfSize(14)
        self.addSubview(_label)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class MessageListLayout: UICollectionViewLayout {
    var _messageIds: [Int] = []
    var _messageOffsets: [CGFloat] = []
    var _messageHeights: [CGFloat] = []
    
    func updateData(messageList: [Message]) {
        _messageOffsets = []
        _messageHeights = []
        _messageIds = []
        var curr: CGFloat = 0
        let textStyle = NSMutableParagraphStyle()
        textStyle.lineBreakMode = .ByWordWrapping
        textStyle.alignment = .Left
        for message in messageList {
            _messageIds.append(message.messageId)
            _messageOffsets.append(curr)
            
            let displayText = NSAttributedString(string: "\(message.messageId) - \(message.content)", attributes: [NSParagraphStyleAttributeName: textStyle,
                NSFontAttributeName: UIFont.systemFontOfSize(14)])
            let rect = displayText.boundingRectWithSize(UIScreen.mainScreen().bounds.size, options: [.UsesLineFragmentOrigin], context: nil)
            let height = rect.height + 10
            _messageHeights.append(height)
            curr += height
        }
    }
    
    override func collectionViewContentSize() -> CGSize {
        NSLog("collectionViewContentSize")
        if _messageHeights.count == 0 {
            return CGSize(width: UIScreen.mainScreen().bounds.width, height: 0)
        }
        let lastIdx = self._messageHeights.count - 1
        return CGSize(width: UIScreen.mainScreen().bounds.width, height: _messageHeights[lastIdx] + _messageOffsets[lastIdx])
    }
    
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var output: [UICollectionViewLayoutAttributes] = []
        NSLog("layoutAttributesForElementsInRect \(rect)")
        for var i = 0; i < _messageOffsets.count; i++ {
            if _messageOffsets[i] + _messageHeights[i] < rect.origin.y {
                continue
            }
            if _messageOffsets[i] > rect.maxY {
                break
            }
            if let attr = layoutAttributesForItemAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) {
                output.append(attr)
            }
        }
        return output
    }
    
    override func layoutAttributesForItemAtIndexPath(indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        NSLog("layoutAttributesForItemAtIndexPath \(indexPath.row)")
        let attr = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
        let idx = indexPath.row
        if idx >= self._messageOffsets.count {
            return nil
        }
        attr.frame = CGRect(x: 0, y: _messageOffsets[idx], width: UIScreen.mainScreen().bounds.width, height: _messageHeights[idx])
        return attr
    }
    
    func offsetForMessage(messageId: Int) -> CGFloat {
        for var i = 0; i < _messageIds.count; i++ {
            if _messageIds[i] == messageId {
                return _messageOffsets[i]
            }
        }
        return 0.0
    }
}

@objc protocol MessageListDelegate {
    func wantsMore()
}

class MessageListView: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate {
    private let _layout = MessageListLayout()
    weak var messageListDelegate: MessageListDelegate?
    
    var data: [Message] = [] {
        didSet {
            _updateData()
        }
    }
    
    init(frame: CGRect) {
        super.init(frame: frame, collectionViewLayout: _layout)
        self.backgroundColor = UIColor.whiteColor()
        self.dataSource = self
        self.registerClass(MessageListCell.self, forCellWithReuseIdentifier: "MessageCell")
        self.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func _updateData() {
        var origOffset: CGFloat = 0.0
        var origMessageId: Int = -1
        let cells = visibleCells()
        if cells.count > 0 {
            origOffset = cells[0].frame.origin.y - contentOffset.y
            origMessageId = (cells[0] as? MessageListCell)?.messageId ?? -1
        }
        _layout.updateData(self.data)
        self.reloadData()
        if origMessageId >= 0 {
            self.setContentOffset(CGPoint(x: 0, y: _layout.offsetForMessage(origMessageId) + origOffset), animated: false)
        }
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        NSLog("numberOfItemsInSection \(self.data.count)")
        return self.data.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        NSLog("cellForItemAtIndexPath \(indexPath.row)")
        let cell = dequeueReusableCellWithReuseIdentifier("MessageCell", forIndexPath: indexPath)
        if let cell = cell as? MessageListCell {
            let item = self.data[indexPath.row]
            cell.messageId = item.messageId
            cell.text = "\(item.messageId) - \(item.content)"
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row <= 5 {
            self.messageListDelegate?.wantsMore()
        }
    }
    
}
