//
//  PhotoEditorView.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 9/22/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit

struct PhotoEditorData {
    var image: UIImage
    var compressed: Bool
    var processing: Bool
}

@objc protocol PhotoConverterDelegate: class {
    optional func processPhoto(image: UIImage)
}

@IBDesignable
class PhotoConverterView: UIView {
    let _imageView = UIImageView()
    let _button = UIButton()
    var _data: PhotoEditorData?
    weak var delegate: PhotoConverterDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        // Grey color as placeholder
        _imageView.backgroundColor = UIColor(red: 0.9, green: 0.8, blue: 0.8, alpha: 1.0)
        self.addSubview(_imageView)
        
        _button.backgroundColor = UIColor.clearColor()
        _button.setTitle("", forState: UIControlState.Normal)
        _button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        _button.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
        _button.enabled = true
        _button.addTarget(self, action: "buttonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(_button)
    }
    
    override func layoutSubviews() {
        _imageView.frame = self.bounds
        _button.sizeToFit()
        _button.center = CGPoint(x: self.bounds.width / 2, y: self.bounds.height - 20)
    }
    
    func setData(data: PhotoEditorData) {
        let oldData = self._data
        self._data = data

        if self._data?.image != oldData?.image {
            NSLog("Image updated")
            self._imageView.image = self._data?.image
        }
        var title = ""
        if _data?.processing ?? false {
            title = "Processing..."
            _button.enabled = false
        } else if _data?.compressed ?? false {
            title = "Compressed WebP"
            _button.enabled = false
        } else {
            title = "Original"
            _button.enabled = true
        }
        _button.setTitle(title, forState: UIControlState.Normal)
        setNeedsLayout()
    }
    
    func buttonClicked(sender: UIButton?) {
        if let image = self._data?.image {
            self.delegate?.processPhoto?(image)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
