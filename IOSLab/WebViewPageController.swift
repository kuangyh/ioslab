//
//  WebViewPageController.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 9/29/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit
import SwiftyJSON

class WebViewPageController: UIViewController {
    var _rootView: WebUIComponent?
    
    override func loadView() {
        let screenBounds = UIScreen.mainScreen().bounds
        let navBarHeight = self.navigationController?.navigationBar.frame.maxY ?? 0
        
        _rootView = WebUIComponent(frame: CGRect(x: 0, y: navBarHeight, width: screenBounds.width, height: screenBounds.height - navBarHeight), entryPoint: "test")
        self.view = _rootView
        _rootView?.scrollView.scrollEnabled = false
        _rootView?.listen(self, selector: "nameDidChanged:", channel: "nameChanged")

    }
    
    func nameDidChanged(data: NSDictionary) {
        NSLog("Received %@", data.description)
        let json = JSON(data)
        _rootView?.send("viewProp", data: ["name": "Aha " + json["name"].stringValue])
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
