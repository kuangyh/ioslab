//
//  MyMapManagerImpl.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 9/29/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import MapKit
import React
import Foundation

class MyMapManager: RCTViewManager {
    override func view() -> UIView! {
        return MKMapView()
    }
}