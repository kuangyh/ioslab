//
//  ZenPhotoLibraryController.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 10/30/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit
import MobileCoreServices

class ZenPhotoLibraryController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    weak var delegate: ZenImagePickerDelegate?
    
    var _systemImagePicker: UIImagePickerController?
    
    override func loadView() {
        self.view = UIView(frame: UIScreen.mainScreen().bounds)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if _systemImagePicker == nil {
            let picker = UIImagePickerController()
            picker.sourceType = .PhotoLibrary
            picker.mediaTypes = [kUTTypeImage as String]
            self.addChildViewController(picker)
            self.view.addSubview(picker.view)
            _systemImagePicker = picker
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        self.delegate?.imageDidTaken(image, fromPhotoLibrary: true)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.delegate?.imagePickerCanceled()
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
