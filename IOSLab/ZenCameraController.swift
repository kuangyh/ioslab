//
//  ZenCameraController.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 10/29/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit
import PromiseKit
import AVFoundation

class ZenCameraController: UIViewController, ZenCameraViewDelegate {
    weak var delegate: ZenImagePickerDelegate?

    private let _cameraPostion: AVCaptureDevicePosition
    private var _captureSession: AVCaptureSession?
    private let _captureOutput = AVCaptureStillImageOutput()
    private let _captureQueue: dispatch_queue_t = dispatch_queue_create("com.wooko.ZenCameraController", nil)
    private var _cameraView: ZenCameraView?
    
    init(cameraPosition: AVCaptureDevicePosition) {
        _cameraPostion = cameraPosition
        super.init(nibName: nil, bundle: nil)
        _captureSession = _createCaptureSession(_captureOutput)
    }
    
    override func loadView() {
        _cameraView = ZenCameraView(frame: UIScreen.mainScreen().bounds)
        _cameraView?.delegate = self
        // TODO(yuheng): what if camera is not allowed?
        self.view = _cameraView
        if let session = _captureSession {
            _cameraView!.previewLayer = AVCaptureVideoPreviewLayer(session: session)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        dispatch_async(self._captureQueue) {
            self._captureSession?.startRunning()
        }
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        dispatch_async(self._captureQueue) {
            self._captureSession?.stopRunning()
        }
        super.viewDidDisappear(animated)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func _createCaptureSession(outputTo: AVCaptureOutput) -> AVCaptureSession? {
        let authorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        if authorizationStatus == .Denied || authorizationStatus == .Restricted {
            NSLog("Camera not allowed :-(")
            return nil
        }
        let session = AVCaptureSession()
        var hasInput: Bool = false
        if let availableCameraDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo) as?[AVCaptureDevice] {
            for device in availableCameraDevices {
                if device.position != _cameraPostion {
                    continue
                }
                guard let deviceInput = try? AVCaptureDeviceInput(device: device) else {
                    continue
                }
                if session.canAddInput(deviceInput) {
                    session.addInput(deviceInput)
                    hasInput = true
                    break
                }
            }
        }
        if !hasInput {
            NSLog("No camera :-(")
            return nil
        }
        if session.canAddOutput(outputTo) {
            session.addOutput(outputTo)
        } else {
            NSLog("Don't allow to add output")
            return nil
        }
        session.sessionPreset = AVCaptureSessionPresetPhoto
        return session
    }

    func zenCameraView(takesPhoto view: ZenCameraView) {
        dispatch_async(self._captureQueue) {
            let conn = self._captureOutput.connectionWithMediaType(AVMediaTypeVideo)
            conn.videoOrientation = AVCaptureVideoOrientation.Portrait
            self._captureOutput.captureStillImageAsynchronouslyFromConnection(conn) {
                buffer, error in
                if error != nil {
                    NSLog("Capture error %@", error)
                    return
                }
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
                
                if let image = UIImage(data: imageData) {
                    self.delegate?.imageDidTaken(image, fromPhotoLibrary: false)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
