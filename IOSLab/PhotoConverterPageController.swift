//
//  PhotoEditorViewController.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 9/22/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import UIKit
import PromiseKit

class PhotoConverterPageController: UIViewController, PhotoConverterDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var _photoEditor: PhotoConverterView?
    
    override func loadView() {
        _photoEditor = PhotoConverterView(frame: UIScreen.mainScreen().bounds)
        _photoEditor?.delegate = self
        self.view = _photoEditor
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        ImageService.INSTANCE.loadImageFromRemote(["IMG_1949-large.JPG", "IMG_1949-middle.JPG"], queue: dispatch_get_main_queue()) {
            name, index, image, error in
            if let img = image {
                self._photoEditor?.setData(PhotoEditorData(image: img, compressed: false, processing: false))
            } else {
                NSLog("Get null image")
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Tap didn't cached by view should triggers toggle navigation bar for this page
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.navigationController?.setNavigationBarHidden(
            !(self.navigationController?.navigationBarHidden ?? false), animated: true)
    }
    
    func processPhoto(orig: UIImage) {
        _photoEditor?.setData(PhotoEditorData(image: orig, compressed: false, processing: true))
        ImageService.INSTANCE.convertToWebPImage(orig, quality: 0.8)
        .then(on: dispatch_get_main_queue()) { (image) -> Void in
            if let converted = image {
                self._photoEditor?.setData(PhotoEditorData(image: converted, compressed: true, processing: false))
            } else {
                NSLog("Convert failed")
                self._photoEditor?.setData(PhotoEditorData(image: orig, compressed: false, processing: false))
            }
        }
    }
}
