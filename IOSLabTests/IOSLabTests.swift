//
//  IOSLabTests.swift
//  IOSLabTests
//
//  Created by Yuheng Kuang on 9/22/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import XCTest
import PromiseKit
@testable import IOSLab

class IOSLabTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMessageService() {
        let service = MessageService.shared
        service.getMessages(2, refMessageId: -1, dir: -1, pageSize: 10, cacheOnly: true).then {
            (result: GetMessageResult) -> Void in
            NSLog("%@", result.hasMore)
            NSLog("%@", result.messages.count)
        }
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
