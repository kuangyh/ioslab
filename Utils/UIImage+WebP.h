//
//  UIImage+WebP.h
//  iOS-WebP
//
//  Created by Sean Ooi on 12/21/13.
//  Copyright (c) 2013 Sean Ooi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebP/decode.h>
#import <WebP/encode.h>

@interface UIImage (WebP)

// Decoding
+ (UIImage*)imageWithWebPData:(NSData*)imgData;

+ (UIImage*)imageWithWebP:(NSString*)filePath;

// Encoding
+ (NSData*)imageToWebP:(UIImage*)image quality:(CGFloat)quality;

+ (NSData*)imageToWebP:(UIImage*)image
               quality:(CGFloat)quality
                preset:(WebPPreset)preset
           configBlock:(void (^)(WebPConfig* config))configBlock
                 error:(NSError**)error;


@end