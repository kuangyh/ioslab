//
//  helpers.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 10/11/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import Foundation

func encodeJSON(data: AnyObject) -> NSString {
    if let data = try? NSJSONSerialization.dataWithJSONObject(data, options: NSJSONWritingOptions()) {
        return NSString(data: data, encoding: NSUTF8StringEncoding) ?? ""
    } else {
        return ""
    }
}

func decodeJSON(data: NSData) -> AnyObject? {
    if let result = try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions()) {
        return result
    }
    return nil
}

func decodeJSON(json: String) -> AnyObject? {
    if let data = json.dataUsingEncoding(NSUTF8StringEncoding) {
        return decodeJSON(data)
    }
    return nil
}

