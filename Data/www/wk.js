// WebUIComponent <-> JS
window.wk = window.wk || {};
(function() {

var channelListeners_ = {};
window.channelListeners_ = channelListeners_;

wk.listen = function(channel, fn) {
  if (!channelListeners_[channel]) {
    channelListeners_[channel] = [];
  }
  channelListeners_[channel].push(fn);
};

wk.onReceivingData = function(data) {
  var channel = data['channel'];
  if (!channel || !channelListeners_[channel]) {
    return;
  }
  var listeners = channelListeners_[channel];
  var payload = data['payload'];
  for (var i = 0; i < listeners.length; i++) {
    try {
      listeners[i](payload);
    } catch (e) {
      console.log("exception while handling data received through " + channel, e);
    }
  }
};

wk.ready = function() {
  if (window.__pending) {
    var pending = __pending;
    delete window.__pending;
    for (var i = 0; i < pending.length; i++) {
      wk.onReceivingData(pending[i]);
    }
  }
  window.__receive = wk.onReceivingData;
};

wk.notify = function(channel, payload) {
  if (webkit && webkit.messageHandlers[channel]) {
    webkit.messageHandlers[channel].postMessage(payload);
  }
}

}).call(window);
