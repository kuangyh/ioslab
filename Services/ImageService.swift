//
//  ImageService.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 9/22/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit
import SDWebImage

typealias ImageDownloadProgress = (chainIndex: Int, name: String, image: UIImage?, error: NSError?) -> Void

// Implementation note:
// - image return order:
//   1. highest quality image in cache,
//   2. any of the higher quality image, it makes sure everytime it returns a better one than before, but doesn't ensure returning all requesting image.
// - further improvment: not so aggresive about downloading, reachability support.
class ImageDownloadTask {
    weak var service: ImageService?
    let chain: [String]
    var progress: ImageDownloadProgress?
    let queue: dispatch_queue_t
    var _completedIndex: Int
    var _pendingDownloads: Int = 0
    
    init(service: ImageService, chain: [String], progress: ImageDownloadProgress?, queue: dispatch_queue_t) {
        self.service = service
        self.chain = chain
        self.progress = progress
        self.queue = queue
        self._completedIndex = chain.count
    }
    
    func start() {
        dispatch_async(self.queue) {
            var cachedIdx = 0
            for cachedIdx = 0; cachedIdx < self.chain.count; cachedIdx++ {
                if self.imageIsCachedAtIndex(cachedIdx) {
                    break
                }
            }
            if cachedIdx < self.chain.count {
                // This should hit progress immediatly
                print("hit cache at index \(cachedIdx), \(self.chain[cachedIdx])")
                self.downloadAIndex(cachedIdx)
            }
            for var i = 0; i < cachedIdx; i++ {
                // Send out all pending download requests
                print("download remote at index \(i), \(self.chain[i])")
                self.downloadAIndex(i)
            }
        }
    }
    
    func imageIsCachedAtIndex(chainIdx: Int) -> Bool {
        let manager = SDWebImageManager.sharedManager()
        return manager.cachedImageExistsForURL(self.service!.urlForImageName(chain[chainIdx]))
    }
    
    func downloadAIndex(chainIdx: Int) {
        let manager = SDWebImageManager.sharedManager()
        let url = self.service!.urlForImageName(chain[chainIdx])
        self._pendingDownloads += 1
        manager.downloadImageWithURL(url, options: SDWebImageOptions(), progress: nil) {
            image, error, cacheType, finished, imageURL in
            dispatch_async(self.queue) {
                self._pendingDownloads -= 1
                if error != nil {
                    if self._pendingDownloads <= 0 && self._completedIndex >= self.chain.count {
                        self.progress?(chainIndex: chainIdx, name: self.chain[chainIdx], image: nil, error: error)
                        self.progress = nil
                    }
                } else {
                    if chainIdx < self._completedIndex {
                        self._completedIndex = chainIdx
                        self.progress?(chainIndex: chainIdx, name: self.chain[chainIdx], image: image, error: nil)
                    }
                    if self._pendingDownloads <= 0 {
                        self.progress = nil
                    }
                }
            }
        }
    }
}

class ImageService {
    static let INSTANCE = ImageService()
    static let REMOTE_IMAGE_BASE_URL = "http://192.168.1.2:8100/Media/photos/"
    
    // Load remote image(s)
    // @param chain: image name for different size/quality of the image from most preferable (highest quality) to least preferable
    // @param queue: the queue progress callback runs on
    // @param progress: this block can be call multiple times, each time providing a better quality image in chain, or an error if all requesting images cannot be obtained.
    func loadImageFromRemote(chain: [String], queue: dispatch_queue_t, progress: ImageDownloadProgress) {
        let task = ImageDownloadTask(service: self, chain: chain, progress: progress, queue: queue)
        task.start()
    }
    
    func urlForImageName(name: String) -> NSURL {
        return NSURL(string: ImageService.REMOTE_IMAGE_BASE_URL + name)!
    }
    
    // TODO(yuheng): change to use SDWebImage's own refresh and prevent fill up whole concurrent download pool.
    func preloadImagesFromRemote(listOfChain: [[String]]) {
        for chain in listOfChain {
            let task = ImageDownloadTask(service: self, chain: chain, progress: nil, queue: dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0))
            task.start()
        }
    }

    // =================
    // Local processing
    // =================
    func loadImageFromBundle(name: String) -> Promise<UIImage?> {
        return dispatch_promise {
            if let path = NSBundle.mainBundle().pathForResource(name, ofType: "jpg", inDirectory: "Data/Images") {
                return UIImage(contentsOfFile: path)
            } else {
                 return nil
            }
        }
    }
    
    func convertToWebPImage(image: UIImage, quality: Float) -> Promise<UIImage?> {
        return dispatch_promise(on: dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
            if let data = UIImage.imageToWebP(image, quality: CGFloat(quality)) {
                return UIImage(webPData: data)
            }
            return nil as UIImage?
        }
    }
}