//
//  MessageService.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 11/8/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import Foundation
import PromiseKit

struct Message {
    let messageId: Int
    let threadId: Int
    let content: String
}

struct MessageListCursor {
    var threadId: Int
    var messageId: Int
    var direction: Int
}

// A mock message service to test collection view design
class MessageService {
    static let shared = MessageService()
    
    // TODO(yuheng): this is a mock
    func getMessages(cursor: MessageListCursor, limit: Int) -> Promise<[Message]> {
        return dispatch_promise {
            var threadId = cursor.threadId
            var messageId = cursor.messageId
            var messageList: [Message] = []
            for var i = 0; i < limit; i++ {
                let message = Message(messageId: messageId, threadId: threadId, content: self._mockContent[messageId % self._mockContent.count])
                messageList.append(message)
                messageId += cursor.direction
                if arc4random() % 10 == 0 {
                    threadId += cursor.direction
                }
                if threadId < 0 || messageId < 0 {
                    break
                }
            }
            if cursor.direction < 0 {
                messageList = messageList.reverse()
            }
            return messageList
        }
    }
    
    private let _mockContent = [
        "Wilt thou be gone? it is not yet near day: It was the nightingale, and not the lark, That pierced the fearful hollow of thine ear;",
        "Nightly she sings on yon pomegranate-tree: Believe me, love, it was the nightingale.",
        "It was the lark, the herald of the morn, No nightingale: look, love, what envious streaks Do lace the severing clouds in yonder east:",
        "Night's candles are burnt out, and jocund day",
        "Stands tiptoe on the misty mountain tops. I must be gone and live, or stay and die.",
        "Yon light is not day-light, I know it, I: It is some meteor that the sun exhales,",
        "To be to thee this night a torch-bearer, And light thee on thy way to Mantua:",
        "Therefore stay yet; thou need'st not to be gone.",
        "Let me be ta'en, let me be put to death;",
        "I am content, so thou wilt have it so. I'll say yon grey is not the morning's eye, 'Tis but the pale reflex of Cynthia's brow;",
        "Nor that is not the lark, whose notes do beat The vaulty heaven so high above our heads:",
        "I have more care to stay than will to go: Come, death, and welcome! Juliet wills it so. How is't, my soul? let's talk; it is not day."]
}