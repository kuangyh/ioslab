//
//  DBService.swift
//  IOSLab
//
//  Created by Yuheng Kuang on 10/10/15.
//  Copyright © 2015 Yuheng Kuang. All rights reserved.
//

import Foundation

// DB service handles creation, migration and queueing of local sqlite storage
// TODO(yuheng): we don't need all these shit if we use core data, but would get another piece of shit instead.
class DBService {
    static var shared = DBService()
    // NOTE: we're working on local db so making eveything happens in one queue doesn't harm
    var queue: FMDatabaseQueue!

    func load(dbname: String) {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let dbPath = documentsURL.URLByAppendingPathComponent(dbname + ".sqlite").path!
        // let dbExists = NSFileManager.defaultManager().fileExistsAtPath(dbPath)
        let dbExists = false
        // TODO: schema versioning and migration
        self.queue = FMDatabaseQueue(path: dbPath)
        if !dbExists  {
            NSLog("New db file, needs create tables")
            createTables()
        }
    }

    func createTables() {
        queue.inDatabase() { db in
            let result = db.executeStatements(
                // Log table: just for test
                "CREATE TABLE IF NOT EXISTS log (timestamp integer, message text);" +
                "CREATE TABLE IF NOT EXISTS feed (item_id int primary key, content_json text);"
            )
            if !result {
                // TODO: panic :-(
                NSLog("Ooooops, create tables failed")
            }
        }
    }

    deinit {
        if self.queue != nil {
            self.queue.close()
        }
    }
}

